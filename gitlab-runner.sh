sudo gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token REGISTRATION_TOKEN \
  --executor docker \
  --description "EC2 Meta-Cortex Group Gitlab Runner" \
  --docker-image "alpine" \
  --docker-privileged \
  --docker-volumes "/certs/client" \
  --tag-list ec2,runner